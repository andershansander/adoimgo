package serialize

import (
	"sync/atomic"

	"bitbucket.org/andershansander/adoimgo/game"
)

var messageIdCounter = int32(0)

type messageLogEntry struct {
	message string
	id      int32
}

func GameStateToJson(s *game.State) map[string]interface{} {
	var update = make(map[string]interface{})
	update["map"] = make(map[string]interface{})
	actors := make([]game.Actor, 0)
	l := game.CurrentLevel(s)
	actorList := l.GetActors()
	for e := actorList.Front(); e != nil; e = e.Next() {
		actors = append(actors, *e.Value.(*game.Actor))
	}
	update["map"].(map[string]interface{})["actors"] = actors
	update["map"].(map[string]interface{})["rows"] = l.GetRows()
	update["map"].(map[string]interface{})["location"] = s.CurrentWorldLocation
	update["player"] = *s.Player

	messages := make([]interface{}, 0)
	for _, message := range s.MessageLog {
		if !message.Secret || true {
			entry := make(map[string]interface{})
			entry["text"] = message.Message
			entry["id"] = atomic.AddInt32(&messageIdCounter, 1)
			entry["secret"] = message.Secret
			messages = append(messages, entry)
		}
	}
	update["messages"] = messages
	return update
}
