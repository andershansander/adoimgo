package action

import "reflect"

func toInt(v interface{}) int {
	switch value := v.(type) {
	case float64:
		return int(value)
	case float32:
		return int(value)
	case int:
		return value
	}
	panic("Could not convert value of type " + reflect.TypeOf(v).Name())
}
