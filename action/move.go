package action

import (
	"errors"
)

type Move struct {
	Target Target
}

func NewMoveAction(targetX, targetY int) Base {
	target := make(map[string]interface{})
	target["x"] = targetX
	target["y"] = targetY
	payload := make(map[string]interface{})
	payload["target"] = target
	return Base{
		Type:    "move",
		Payload: payload,
	}
}
func ToMoveAction(action Base) (Move, error) {
	if action.Type != "move" {
		panic("Action of type " + action.Type + " cannot be parsed as a MoveAction")
	}
	target, ok := action.Payload["target"]
	if !ok {
		return Move{}, errors.New("Move action is missing a target")
	}

	var x = toInt(target.(map[string]interface{})["x"])
	var y = toInt(target.(map[string]interface{})["y"])

	return Move{
		Target: Target{
			X: x,
			Y: y,
		}}, nil
}
