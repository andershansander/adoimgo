package action

import (
	"errors"
)

type Melee struct {
	Target Target
}

func NewMeleeAction(targetX, targetY int) Base {
	target := make(map[string]interface{})
	target["x"] = targetX
	target["y"] = targetY
	payload := make(map[string]interface{})
	payload["target"] = target
	return Base{
		Type:    "melee",
		Payload: payload,
	}
}
func ToMeleeAction(action Base) (Melee, error) {
	if action.Type != "melee" {
		panic("Action of type " + action.Type + " cannot be parsed as a MoveAction")
	}
	target, ok := action.Payload["target"]
	if !ok {
		return Melee{}, errors.New("Melee action is missing a target")
	}

	var x = toInt(target.(map[string]interface{})["x"])
	var y = toInt(target.(map[string]interface{})["y"])

	return Melee{
		Target: Target{
			X: x,
			Y: y,
		}}, nil
}
