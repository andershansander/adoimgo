package actionhandler

import (
	"testing"

	"bitbucket.org/andershansander/adoimgo/action"
)

func TestParseActionBaseType(t *testing.T) {
	jsonData := []byte(`
        {
            "type": "some type"
        }`)
	baseAction, _ := action.ToAction(jsonData)
	if baseAction.Type != "some type" {
		t.Error("action type expected to be 'some type' not ", baseAction.Type)
	}
}

func TestParseMoveAction(t *testing.T) {
	jsonData := []byte(`
        {
            "type": "move",
            "payload": {
                 "target": {
					 "x": 5,
					 "y": 4
				 }
            }
        }`)
	baseAction, _ := action.ToAction(jsonData)
	moveAction, _ := action.ToMoveAction(baseAction)

	expectedTarget := action.Target{X: 5, Y: 4}

	if moveAction.Target.X != expectedTarget.X || moveAction.Target.Y != expectedTarget.Y {
		t.Error("action payload expected to be X: 5, Y: 4 not ", moveAction.Target)
	}
}

func TestParseInvalidMoveAction(t *testing.T) {
	jsonData := []byte(`
        {
            "type": "move",
            "payload": {
                 "direction": "nw"
            }
        }`)
	baseAction, _ := action.ToAction(jsonData)
	_, err := action.ToMoveAction(baseAction)

	if err == nil {
		t.Error("Expecting parse error")
	}
}
