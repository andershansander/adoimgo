package action

import (
	"encoding/json"
	"log"
)

type Target struct {
	X, Y, Z int
}

type Base struct {
	Type    string
	Payload map[string]interface{}
}

func ToAction(jsonAction []byte) (Base, error) {
	action := Base{}
	log.Println("Parsing action " + string(jsonAction))
	error := json.Unmarshal(jsonAction, &action)
	if error != nil {
		log.Println("Could not parse user input as an action", error)
	}
	return action, error
}
