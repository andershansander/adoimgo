package action

type Wait struct {
}

func NewWaitAction() Base {
	return Base{
		Type: "wait",
	}
}

func ToWaitAction(action Base) Wait {
	if action.Type != "wait" {
		panic("Action of type " + action.Type + " cannot be parsed as a WaitAction")
	}
	return Wait{}
}
