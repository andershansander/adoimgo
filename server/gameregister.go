package server

import (
	"sync"

	"bitbucket.org/andershansander/adoimgo/game"
)

var gameLibrary = make(map[game.GameID]game.Game)

var mut = sync.Mutex{}

func RegisterGame(g game.Game) {
	mut.Lock()
	if _, exist := gameLibrary[g.ID()]; exist {
		panic("Cannot register the same game twice")
	}
	gameLibrary[g.ID()] = g
	mut.Unlock()
}

func FindGame(gameID game.GameID) game.Game {
	mut.Lock()
	g := gameLibrary[gameID]
	mut.Unlock()
	return g
}
