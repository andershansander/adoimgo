package server

import (
	"fmt"
	"log"

	"bitbucket.org/andershansander/adoimgo/action"
	"bitbucket.org/andershansander/adoimgo/game"
	"bitbucket.org/andershansander/adoimgo/serialize"
	"github.com/gorilla/websocket"
)

func CreateClient(ws *websocket.Conn, gameID game.GameID, observer bool) {
	client := &clientType{
		websocket:         ws,
		gameID:            gameID,
		observer:          observer,
		connectionClosedC: make(chan bool),
		updateC:           make(chan map[string]interface{}),
	}
	go client.listen()
	go client.send()
}

type clientType struct {
	websocket         *websocket.Conn
	gameID            game.GameID
	observer          bool
	connectionClosedC chan bool
	updateC           chan map[string]interface{}
}

func (c *clientType) OnChange(s map[string]interface{}) {
	c.updateC <- s
}

func (c *clientType) send() {
	FindGame(c.gameID).RegisterListener(c, serialize.GameStateToJson)
L:
	for {
		select {
		case <-c.connectionClosedC:
			break L
		case s := <-c.updateC:
			err := c.websocket.WriteJSON(s)
			if err != nil {
				log.Println("Could not send update to connection:", err)
			}
		}
	}
	log.Println("Connection closed. Exiting client send loop")
}

func (c *clientType) listen() {
	for {
		_, message, err := c.websocket.ReadMessage()
		if err != nil {
			log.Println("Could not read from connection.", err)
			select {
			case c.connectionClosedC <- true:
			default:
				log.Println("Connection closed but no one was listening to close events")
			}
			break
		}
		userAction, err := action.ToAction(message)
		if err != nil {
			log.Println("Could not parse input from user. Ignoring the command:"+string(message), err)
		} else {
			select {
			case FindGame(c.gameID).UserCommandC() <- userAction:
			default:
				fmt.Println("Channel full. Discarding user input:" + string(message))
			}
		}
	}
}
