package server

import (
	"log"
	"net/http"
	"strconv"

	"bitbucket.org/andershansander/adoimgo/game"
	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{CheckOrigin: func(r *http.Request) bool { return true }}

func onConnection(res http.ResponseWriter, req *http.Request) {
	log.Println("New http connection!")
	conn, err := upgrader.Upgrade(res, req, nil)
	if err != nil {
		log.Println("Failed to upgrade connection to websocket:", err)
		http.NotFound(res, req)
		return
	}
	var g game.Game
	if hasParameter(req, "gameid") {
		gameIDParam := req.URL.Query().Get("gameid")
		log.Println("New connection for game ", gameIDParam)
		gid, err := strconv.Atoi(gameIDParam)
		if err != nil {
			log.Println("Error converting gameID: "+gameIDParam, err)
			res.WriteHeader(http.StatusBadRequest)
			return
		}
		g := FindGame(game.GameID(gid))
		if g == nil {
			log.Println("Bad game id: " + gameIDParam)
			http.NotFound(res, req)
			return
		}
	} else {
		g = game.CreateGame()
		RegisterGame(g)
		log.Println("New game created with id", g.ID())
	}
	observer := hasParameter(req, "observer")
	CreateClient(conn, g.ID(), observer)
}

func hasParameter(req *http.Request, name string) bool {
	return len(req.URL.Query().Get(name)) > 0
}

func serveHTML(res http.ResponseWriter, req *http.Request) {
	http.ServeFile(res, req, "index.html")
}

/*StartServer starts a websocket server listening on the given port*/
func StartServer(port int) {
	http.HandleFunc("/ws", onConnection)
	http.HandleFunc("/", serveHTML)
	http.ListenAndServe(":"+strconv.Itoa(port), nil)
}
