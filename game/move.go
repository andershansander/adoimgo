package game

import (
	"bitbucket.org/andershansander/adoimgo/action"
)

func performMove(act action.Move, s *State) {
	l := CurrentLevel(s)
	actor := l.NextToAct()
	target := Location{X: act.Target.X, Y: act.Target.Y}
	if l.IsWalkable(target) && Adjacent(target, actor.Location) {
		occupied, _ := l.SpaceOccupied(target)
		if !occupied {
			actor.Location = target
			actor.Time += 1000
		}
	} else if l.IsOutsideMap(target) {
		wL := WorldLocation{
			X: s.CurrentWorldLocation.X,
			Y: s.CurrentWorldLocation.Y,
			Z: s.CurrentWorldLocation.Z,
		}
		newLoc := Location{
			X: actor.Location.X,
			Y: actor.Location.Y,
		}
		if target.X < 0 {
			wL.X = s.CurrentWorldLocation.X - 1
		} else if target.X >= l.Width() {
			wL.X = s.CurrentWorldLocation.X + 1
			newLoc.X = 0
		}
		if target.Y < 0 {
			wL.Y = s.CurrentWorldLocation.Y - 1
		} else if target.Y >= l.Height() {
			wL.Y = s.CurrentWorldLocation.Y + 1
			newLoc.Y = 0
		}
		if !s.World.HasMapAt(wL) {
			s.World.InsertLocationAt(wL, generateLevel(wL))
		}
		l.RemoveActor(actor)
		s.World.GetMapAt(wL).AddActor(actor)
		s.CurrentWorldLocation = wL
		if target.X < 0 {
			newLoc.X = s.World.GetMapAt(wL).Width() - 1
		}
		if target.Y < 0 {
			newLoc.Y = s.World.GetMapAt(wL).Height() - 1
		}
		actor.Location = newLoc
	}
}
