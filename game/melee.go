package game

import (
	"log"

	"bitbucket.org/andershansander/adoimgo/action"
)

func performMelee(act action.Melee, s *State) {
	l := CurrentLevel(s)
	actor := l.NextToAct()
	target := Location{X: act.Target.X, Y: act.Target.Y}
	hasTarget, targetActor := l.SpaceOccupied(target)
	if hasTarget && Adjacent(target, actor.Location) {
		s.MessageLog = append(s.MessageLog, Event{
			Message: actor.Sign + " attacks " + targetActor.Sign,
			Secret:  !s.Player.CanSee(actor.Location) || s.Player.CanSee(target),
		})
		actor.Time += 1000
	} else {
		if !Adjacent(target, actor.Location) {
			log.Println("Cannot melee non-adjacent square")
		} else if !hasTarget {
			log.Println("No melee target at target location")
		} else {
			log.Println("Could not perform melee due to unknown reason")
		}
	}
}
