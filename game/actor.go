package game

import "bitbucket.org/andershansander/adoimgo/action"

type Behaviour interface {
	NextAction(*Actor, *State) action.Base
}

type ActorID int32

type Actor struct {
	Id            ActorID
	Location      Location `json:"location"`
	Time          int64    `json:"time"`
	Sign          string   `json:"sign"`
	AI            Behaviour
	attributes    *actorAttributes
	npcAttributes *npcAttributes
	memory        *memory
	speed         int
}

type npcAttributes struct {
	Alertness      float32
	Aggressitivity float32
}
type actorAttributes struct {
	MaxHealth  int
	Health     int
	Perception int
	Sneakiness int
}

func (a *Actor) CanSee(l Location) bool {
	sightRadius := a.attributes.Perception / 25
	return BirdDistance(a.Location, l) < sightRadius
}

func (a *Actor) TravelCost(terrain rune) int {
	return 1000 - a.speed
}

func (a *Actor) CanTravelOn(terrain rune) bool {
	return terrain != 'W' && terrain != '#'
}
