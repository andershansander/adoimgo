package game

import (
	"bitbucket.org/andershansander/adoimgo/game/levels"
	"bitbucket.org/andershansander/adoimgo/game/levels/terrain"
)

func ToLevel(rows []string) Level {
	return &level{
		rows: rows,
	}
}

func generateLevel(wL WorldLocation) Level {
	switch wL {
	case WorldLocation{X: 0, Y: 100, Z: 0}:
		l := &level{
			rows: levels.GenerateStartLevel(),
		}
		l.AddActor(createUnicorn(4, 4))
		l.AddActor(createBandit(3, 3))
		return l
	default:
		options := levels.Options{
			Ocean: levels.Ocean{
				North: wL.Y == 0 && wL.Z == 0,
				South: wL.Y == 199 && wL.Z == 0,
				West:  wL.X == 0 && wL.Z == 0,
				East:  wL.X == 199 && wL.Z == 0,
			},
			TerrainType: terrain.Forrest,
		}
		return &level{
			rows: levels.Generate(options),
		}
	}
}
