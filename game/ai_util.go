package game

import (
	"math/rand"
	"time"

	"bitbucket.org/andershansander/adoimgo/action"
	"bitbucket.org/andershansander/adoimgo/game/pathfinding"
)

func getPossibleMoves(actor *Actor, state *State) []action.Base {
	possibleActions := make([]action.Base, 0, 9)
	for xDiff := -1; xDiff < 2; xDiff++ {
		for yDiff := -1; yDiff < 2; yDiff++ {
			if xDiff != 0 || yDiff != 0 {
				loc := Location{
					X: actor.Location.X - xDiff,
					Y: actor.Location.Y - yDiff,
				}
				if CurrentLevel(state).IsWalkable(loc) {
					possibleActions = append(possibleActions, action.NewMoveAction(loc.X, loc.Y))
				}
			}
		}
	}
	return possibleActions
}

func getNextMoveTowards(actor *Actor, state *State, target Location) action.Base {
	if actor.Location.X == target.X && actor.Location.Y == target.Y {
		return action.NewWaitAction()
	}
	l := CurrentLevel(state)
	squares := make([][]pathfinding.Square, len(l.GetRows()))
	for y, row := range l.GetRows() {
		squares[y] = make([]pathfinding.Square, len(row))
		for x, square := range row {
			squares[y][x] = pathfinding.Square{
				Traversable:  actor.CanTravelOn(square),
				MovementCost: actor.TravelCost(square),
				Heuristic:    BirdDistance(Location{X: x, Y: y}, target) * 1000,
				X:            x,
				Y:            y,
			}
		}
	}
	found, path := pathfinding.CreatePath(squares, pathfinding.StartPoint(actor.Location.X, actor.Location.Y), pathfinding.Target(target.X, target.Y))
	if found {
		return action.NewMoveAction(path[0].X(), path[0].Y())
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	possibleActions := getPossibleMoves(actor, state)
	return possibleActions[r.Intn(len(possibleActions))]
}
