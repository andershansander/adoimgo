package game

import (
	"math/rand"
	"time"
)

func detectVsSneakCheck(a *Actor, target *Actor) bool {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))

	alertnessBonus := random.Float32() * (a.npcAttributes.Alertness - float32(5)) * float32(a.attributes.Perception) * alertnessWeight

	detectRoll := random.Float32()*float32(a.attributes.Perception) + alertnessBonus
	sneakRoll := random.Float32() * float32(target.attributes.Sneakiness)
	return detectRoll > sneakRoll
}
