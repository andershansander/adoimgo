package game

type Event struct {
	Message string
	Secret  bool
}
