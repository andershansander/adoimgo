package game

import (
	"log"
	"math/rand"
	"time"

	"bitbucket.org/andershansander/adoimgo/action"
)

type hostileBehaviour struct {
}

func (f hostileBehaviour) NextAction(actor *Actor, state *State) action.Base {
	possibleActions := make([]action.Base, 0, 9)
	if actor.memory.KnowsAbout(state.Player) {
		if actor.memory.HaveNoticed(state.Player) && BirdDistance(actor.Location, state.Player.Location) <= 1 {
			possibleActions = append(possibleActions, action.NewMeleeAction(state.Player.Location.X, state.Player.Location.Y))
		} else {
			possibleActions = append(possibleActions, getNextMoveTowards(actor, state, actor.memory.otherActors[state.Player.Id].location))
			log.Println("Chase - player is at " + state.Player.Location.toString() + " and the AI is at " + actor.Location.toString())
		}
	} else {
		possibleActions = append(possibleActions, action.NewWaitAction())
		possibleActions = append(possibleActions, getPossibleMoves(actor, state)...)
	}
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return possibleActions[r.Intn(len(possibleActions))]
}
