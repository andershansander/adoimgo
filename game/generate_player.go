package game

import (
	"math/rand"
	"time"
)

func createPlayer(x, y int) *Actor {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	hp := random.Intn(50)

	return &Actor{
		Location: Location{
			X: x,
			Y: y,
		},
		Sign: "@",
		attributes: &actorAttributes{
			Health:    75 + hp,
			MaxHealth: 75 + hp,

			Perception: random.Intn(50) + 75,
			Sneakiness: random.Intn(50) + 75,
		},
		speed: 100,
	}
}
