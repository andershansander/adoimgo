package game

import (
	"math/rand"
	"sync/atomic"
	"time"
)

var npcIdCounter = int32(0)

func createActor(x, y int) *Actor {
	return &Actor{
		Id: ActorID(atomic.AddInt32(&gameIdCounter, 1)),
		Location: Location{
			X: x,
			Y: y,
		},
		memory: createEmptyMemory(),
	}
}

func createBandit(x, y int) *Actor {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	hp := random.Intn(50)
	a := createActor(x, y)
	a.Sign = "@"
	a.AI = hostileBehaviour{}
	a.attributes = &actorAttributes{
		Health:    75 + hp,
		MaxHealth: 75 + hp,

		Perception: random.Intn(50) + 75,
		Sneakiness: random.Intn(50) + 75,
	}
	a.npcAttributes = &npcAttributes{
		Alertness:      random.Float32() * 10,
		Aggressitivity: random.Float32()*5 + 5,
	}
	return a
}

func createUnicorn(x, y int) *Actor {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	hp := random.Intn(100)
	a := createActor(x, y)
	a.Sign = "U"
	a.AI = friendlyBehaviour{}
	a.attributes = &actorAttributes{
		Health:    100 + hp,
		MaxHealth: 100 + hp,

		Perception: random.Intn(100) + 125,
		Sneakiness: random.Intn(50) + 75,
	}
	a.npcAttributes = &npcAttributes{
		Alertness:      random.Float32() * 10,
		Aggressitivity: random.Float32(),
	}

	return a
}
