package game

import (
	"sync/atomic"

	"sync"

	"bitbucket.org/andershansander/adoimgo/action"
)

var gameIdCounter = int32(0)

func createGame() Game {
	worldLocation := WorldLocation{X: 0, Y: 100, Z: 0}
	gameWorld := createWorld()
	gameWorld.InsertLocationAt(
		worldLocation,
		generateLevel(worldLocation))
	player := createPlayer(5, 5)
	gameWorld.GetMapAt(worldLocation).AddActor(player)

	g := &game{
		state: &State{
			World:                gameWorld,
			Player:               player,
			CurrentWorldLocation: worldLocation,
			MessageLog:           make([]Event, 0),
		},
		id:             GameID(atomic.AddInt32(&gameIdCounter, 1)),
		userCommandC:   make(chan action.Base, 100),
		listeners:      make(map[GameChangeListener]func(*State) map[string]interface{}),
		listenersMutex: sync.Mutex{},
	}

	go g.gameLoop()

	return g
}
