package game

import (
	"log"

	"bitbucket.org/andershansander/adoimgo/action"
)

func perform(act action.Base, s *State) {
	switch act.Type {
	case "move":
		moveAction, err := action.ToMoveAction(act)
		if err == nil {
			performMove(moveAction, s)
		} else {
			log.Println("Ignoring invalid move action from user: ", err)
		}
	case "wait":
		performWait(action.ToWaitAction(act), s)
	case "melee":
		meleeAction, err := action.ToMeleeAction(act)
		if err == nil {
			performMelee(meleeAction, s)
		} else {
			log.Println("Ignoring invalid melee action from user: ", err)
		}
	default:
		log.Println("Bad action type " + act.Type)
	}
}
