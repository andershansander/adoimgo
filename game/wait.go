package game

import (
	"bitbucket.org/andershansander/adoimgo/action"
)

func performWait(act action.Wait, s *State) {
	CurrentLevel(s).NextToAct().Time += 1000
}
