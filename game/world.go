package game

import "strconv"

type WorldLocation struct {
	X int `json:"x"`
	Y int `json:"y"`
	Z int `json:"z"`
}

func ToString(wL WorldLocation) string {
	return "WorldLocation{X: " + strconv.Itoa(wL.X) + ", Y: " + strconv.Itoa(wL.Y) + ", Z: " + strconv.Itoa(wL.Z) + "}"
}

type World interface {
	HasMapAt(location WorldLocation) bool
	GetMapAt(location WorldLocation) Level
	InsertLocationAt(location WorldLocation, m Level)
}

func createWorld() World {
	return &world{levels: make(map[int]map[int]map[int]Level)}
}

type world struct {
	levels map[int]map[int]map[int]Level
}

/*IsIndexed returns true if there is a generated map stored at the given location*/
func (w *world) HasMapAt(location WorldLocation) bool {
	x, y, z := location.X, location.Y, location.Z
	return w.levels[x] != nil && w.levels[x][y] != nil && w.levels[x][y][z] != nil
}

/*InsertLocationAt inserts a map at the given position*/
func (w *world) InsertLocationAt(location WorldLocation, m Level) {
	x, y, z := location.X, location.Y, location.Z
	if w.levels[x] == nil {
		w.levels[x] = make(map[int]map[int]Level)
	}
	if w.levels[x][y] == nil {
		w.levels[x][y] = make(map[int]Level)
	}
	w.levels[x][y][z] = m
}

/*GetMapAt fetches the map at the given position. Expects IsIndexed to return true for the given position*/
func (w *world) GetMapAt(location WorldLocation) Level {
	x, y, z := location.X, location.Y, location.Z
	return w.levels[x][y][z]
}
