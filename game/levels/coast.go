package levels

import (
	"math"
)

func insertOcean(ocean Ocean, source [][]rune) [][]rune {
	shallowWaterFromX, shallowWaterFromY, shallowWaterToX, shallowWaterToY := 0, 0, expectedWidth, expectedHeight
	var deepWaterFromX, deepWaterToX, deepWaterFromY, deepWaterToY int
	if ocean.North {
		deepWaterFromX, deepWaterToX, deepWaterFromY, deepWaterToY = 0, expectedWidth, 0, 4
		shallowWaterFromY = 1
	}
	if ocean.South {
		deepWaterFromX, deepWaterToX, deepWaterFromY, deepWaterToY = 0, expectedWidth, expectedHeight-4, expectedHeight
		shallowWaterToY = expectedHeight - 1
	}
	if ocean.East {
		deepWaterFromX, deepWaterToX, deepWaterFromY, deepWaterToY = expectedWidth-4, expectedWidth, 0, expectedHeight
		shallowWaterToX = expectedWidth - 1
	}
	if ocean.West {
		deepWaterFromX, deepWaterToX, deepWaterFromY, deepWaterToY = 0, 4, 0, expectedHeight
		shallowWaterFromX = 1
	}

	//deep water
	for x := 0; x < expectedWidth; x++ {
		for y := 0; y < expectedHeight; y++ {
			if x >= deepWaterFromX && x < deepWaterToX && y >= deepWaterFromY && y < deepWaterToY {
				source[y][x] = 'W'
			}
		}
	}

	var result = make([][]rune, len(source))
	copy(result, source)
	//shallow water
	for x := shallowWaterFromX; x < shallowWaterToX; x++ {
		for y := shallowWaterFromY; y < shallowWaterToY; y++ {
			oceanNearbyCount := 0
			for nearX := int(math.Max(float64(x-1), 0)); nearX < int(math.Min(float64(x+2), float64(expectedWidth))); nearX++ {
				for nearY := int(math.Max(float64(y-1), 0)); nearY < int(math.Min(float64(y+2), float64(expectedHeight))); nearY++ {
					if source[nearY][nearX] == 'W' {
						oceanNearbyCount++
					}
				}
			}
			if oceanNearbyCount > 0 {
				//get a number between 6 and 0
				shallowThreshold := int(math.Abs(float64(oceanNearbyCount - 3)))
				if random.Intn(8) > shallowThreshold {
					result[y][x] = 'w'
				}
			}
		}
	}
	return result
}
