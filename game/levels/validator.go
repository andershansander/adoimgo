package levels

import "strconv"

var expectedHeight = 14
var expectedWidth = 34

func validate(rows []string) {
	if len(rows) != expectedHeight {
		panic("Level did not have the expected height " + strconv.Itoa(expectedHeight) + ", was " + strconv.Itoa(len(rows)))
	}
	for rowNumber, row := range rows {
		if len(row) != expectedWidth {
			panic("Level did not have the expected width (" + strconv.Itoa(expectedWidth) + ") on row " + strconv.Itoa(rowNumber) + ", was " + strconv.Itoa(len(row)))
		}
	}
}
