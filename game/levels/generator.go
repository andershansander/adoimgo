package levels

import (
	"math/rand"
	"time"

	"bitbucket.org/andershansander/adoimgo/game/levels/static"
	"bitbucket.org/andershansander/adoimgo/game/levels/terrain"
)

func GenerateStartLevel() []string {
	validate(static.StartLevel)
	return static.StartLevel
}

type Ocean struct {
	North bool
	South bool
	West  bool
	East  bool
}

var random = rand.New(rand.NewSource(time.Now().UnixNano()))

type Options struct {
	TerrainType terrain.Type
	Ocean       Ocean
}

func Generate(options Options) []string {
	lvl := createEmptyGrassMap()
	insertTrees(options.TerrainType, lvl)
	lvl = insertOcean(options.Ocean, lvl)
	result := make([]string, expectedHeight)
	for index, row := range lvl {
		result[index] = string(row)
	}
	validate(result)
	return result
}

func insertTrees(terrainType terrain.Type, source [][]rune) {
	for y := 1; y < expectedHeight-1; y++ {
		for x := 1; x < expectedWidth-1; x++ {
			if (terrainType == terrain.Forrest && random.Intn(20) > 15) || random.Intn(20) > 19 {
				source[y][x] = 'T'
			}
		}
	}
}

func createEmptyGrassMap() [][]rune {
	level := make([][]rune, expectedHeight)
	for y := 0; y < expectedHeight; y++ {
		level[y] = make([]rune, expectedWidth)
		for x := 0; x < expectedWidth; x++ {
			if random.Intn(10) > 7 {
				level[y][x] = '.'
			} else {
				level[y][x] = ','
			}
		}
	}
	return level
}
