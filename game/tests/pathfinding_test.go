package gametests

import (
	"testing"

	"bitbucket.org/andershansander/adoimgo/game"
	"bitbucket.org/andershansander/adoimgo/game/pathfinding"
)

func TestFindSimplestPath(t *testing.T) {
	squares := make([][]pathfinding.Square, 3)
	for y := 0; y < 3; y++ {
		squares[y] = make([]pathfinding.Square, 3)
		for x := 0; x < 3; x++ {
			squares[y][x] = pathfinding.Square{
				X:            x,
				Y:            y,
				Traversable:  true,
				Heuristic:    game.BirdDistance(game.Location{X: x, Y: y}, game.Location{X: 2, Y: 2}),
				MovementCost: 1000,
			}

		}
	}
	found, path := pathfinding.CreatePath(squares, pathfinding.StartPoint(0, 0), pathfinding.Target(2, 2))
	if !found {
		t.Error("Expected a path to be found")
	}
	if path[0].X() != 1 || path[0].Y() != 1 {
		t.Error("Expected the shortest possible path")
	}
}

func TestMoveAroundBlockedCenter(t *testing.T) {
	squares := make([][]pathfinding.Square, 3)
	for y := 0; y < 3; y++ {
		squares[y] = make([]pathfinding.Square, 3)
		for x := 0; x < 3; x++ {
			squares[y][x] = pathfinding.Square{
				X:            x,
				Y:            y,
				Traversable:  x != 1 || y != 1,
				Heuristic:    game.BirdDistance(game.Location{X: x, Y: y}, game.Location{X: 2, Y: 2}),
				MovementCost: 1000,
			}

		}
	}
	found, path := pathfinding.CreatePath(squares, pathfinding.StartPoint(0, 0), pathfinding.Target(2, 2))
	if !found {
		t.Error("Expected a path to be found")
	}
	if !(path[0].X() == 0 && path[0].Y() == 1 && path[1].X() == 1 && path[1].Y() == 2 && path[2].X() == 2 && path[2].Y() == 2) && !(path[0].X() == 1 && path[0].Y() == 0 && path[1].X() == 2 && path[1].Y() == 1 && path[2].X() == 2 && path[2].Y() == 2) {
		t.Error("Expected one of the two paths around the center")
	}
}
