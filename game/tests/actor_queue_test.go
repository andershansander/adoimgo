package gametests

import (
	"testing"

	"bitbucket.org/andershansander/adoimgo/game"
)

func testLevel() game.Level {
	return game.ToLevel([]string{
		"####",
		"#..#",
		"#..#",
		"####",
	})
}

func TestOrderShouldSwitchWhenActorActAndTimeBecomeEqual(t *testing.T) {
	l := testLevel()
	a1 := &game.Actor{
		Time: 1,
	}
	a2 := &game.Actor{
		Time: 2,
	}
	l.AddActor(a1)
	l.AddActor(a2)

	next := l.NextToAct()
	if next != a1 {
		t.Error("Expected a1 to be next to act")
	}
	a1.Time = a2.Time
	next = l.NextToAct()
	if next != a2 {
		t.Error("Expected a2 to be next to act")
	}
}

func TestOrderShouldSwitchWhenActorActAndTimeBecomeLargerThanSecondActors(t *testing.T) {
	l := testLevel()
	a1 := &game.Actor{
		Time: 1,
	}
	a2 := &game.Actor{
		Time: 2,
	}
	l.AddActor(a1)
	l.AddActor(a2)

	next := l.NextToAct()
	if next != a1 {
		t.Error("Expected a1 to be next to act")
	}
	a1.Time = a2.Time + 1
	next = l.NextToAct()
	if next != a2 {
		t.Error("Expected a2 to be next to act")
	}
}

func TestOrderShouldNotSwitchWhenActorActAndTimeStillGotTime(t *testing.T) {
	l := testLevel()
	a1 := &game.Actor{
		Time: 1,
	}
	a2 := &game.Actor{
		Time: 3,
	}
	l.AddActor(a1)
	l.AddActor(a2)

	next := l.NextToAct()
	if next != a1 {
		t.Error("Expected a1 to be next to act")
	}
	a1.Time = 2
	next = l.NextToAct()
	if next != a1 {
		t.Error("Still expected a1 to be next to act")
	}
}

func TestOrderShouldNotSwitchWhenActorHaveDoneNothing(t *testing.T) {
	l := testLevel()
	a1 := &game.Actor{
		Time: 1,
	}
	a2 := &game.Actor{
		Time: 1,
	}
	l.AddActor(a1)
	l.AddActor(a2)

	next := l.NextToAct()
	if next != a1 {
		t.Error("Expected a1 to be next to act")
	}
	next = l.NextToAct()
	if next != a1 {
		t.Error("Still expected a1 to be next to act")
	}
}
