package game

import (
	"math"
)

func Adjacent(loc1, loc2 Location) bool {
	return math.Abs(float64(loc1.X-loc2.X)) <= 1 && math.Abs(float64(loc1.Y-loc2.Y)) <= 1
}

func BirdDistance(loc1, loc2 Location) int {
	return int(math.Max(math.Abs(float64(loc1.X-loc2.X)), math.Abs(float64(loc1.Y-loc2.Y))))
}
