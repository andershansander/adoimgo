package game

import (
	"fmt"
	"log"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/andershansander/adoimgo/action"
)

type GameID int
type GameChangeListener interface {
	OnChange(map[string]interface{})
}

type Game interface {
	ID() GameID
	UserCommandC() chan action.Base
	RegisterListener(GameChangeListener, func(*State) map[string]interface{})
	UnregisterListener(GameChangeListener)
}

type Location struct {
	X int `json:"x"`
	Y int `json:"y"`
}

func (l Location) toString() string {
	return "x: " + strconv.Itoa(l.X) + ", y: " + strconv.Itoa(l.Y)
}

type State struct {
	World                World
	Player               *Actor
	CurrentWorldLocation WorldLocation
	MessageLog           []Event
}

func CurrentLevel(s *State) Level {
	return s.World.GetMapAt(s.CurrentWorldLocation)
}

func CreateGame() Game {
	return createGame()
}

type game struct {
	id             GameID
	userCommandC   chan action.Base
	state          *State
	listeners      map[GameChangeListener]func(*State) map[string]interface{}
	listenersMutex sync.Mutex
}

func (g *game) ID() GameID                     { return g.id }
func (g *game) UserCommandC() chan action.Base { return g.userCommandC }

func (g *game) RegisterListener(l GameChangeListener, serialize func(*State) map[string]interface{}) {
	g.listenersMutex.Lock()
	g.listeners[l] = serialize
	g.listenersMutex.Unlock()
}
func (g *game) UnregisterListener(l GameChangeListener) {
	g.listenersMutex.Lock()
	delete(g.listeners, l)
	g.listenersMutex.Unlock()
}

func (g *game) gameLoop() {
	for {
		startTime := time.Now().UnixNano()
		nextActor := CurrentLevel(g.state).NextToAct()
		if nextActor == g.state.Player {
			log.Println("Player is next to act")
			userAction, _ := <-g.userCommandC
			startTime = time.Now().UnixNano()
			perform(userAction, g.state)
		} else if nextActor != nil {
			events := lookAround(nextActor, g.state, detectVsSneakCheck)
			g.state.MessageLog = append(g.state.MessageLog, events...)
			aiAction := nextActor.AI.NextAction(nextActor, g.state)
			log.Println("The AI decided to " + aiAction.Type)
			perform(aiAction, g.state)
		}
		g.listenersMutex.Lock()
		for listener, stateSerializer := range g.listeners {
			listener.OnChange(stateSerializer(g.state))
		}
		g.listenersMutex.Unlock()
		loopDuration := (time.Now().UnixNano() - startTime) / 1000000
		if loopDuration > 50 {
			fmt.Println("Gameloop took " + strconv.FormatInt(loopDuration, 10) + "ms")
		}
	}
}
