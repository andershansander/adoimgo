package game

import (
	"math/rand"
	"time"

	"bitbucket.org/andershansander/adoimgo/action"
)

type friendlyBehaviour struct {
}

func (f friendlyBehaviour) NextAction(actor *Actor, state *State) action.Base {
	possibleActions := make([]action.Base, 0, 9)
	possibleActions = append(possibleActions, action.NewWaitAction())
	possibleActions = append(possibleActions, getPossibleMoves(actor, state)...)
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return possibleActions[r.Intn(len(possibleActions))]
}
