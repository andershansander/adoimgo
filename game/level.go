package game

import (
	"container/list"
	"errors"
)

type Level interface {
	GetRows() []string
	GetActors() list.List
	AddActor(actor *Actor)
	RemoveActor(actor *Actor) error
	NextToAct() *Actor
	Width() int
	Height() int
	HasWallAt(location Location) bool
	IsOutsideMap(location Location) bool
	IsWalkable(location Location) bool
	SpaceOccupied(location Location) (bool, *Actor)
	HasDeepWaterAt(location Location) bool
	HasShallowWaterAt(location Location) bool
	SolidGroundAt(location Location) bool
}

type lastReschedule struct {
	time  int64
	actor *Actor
}
type level struct {
	rows           []string
	actors         list.List
	lastReschedule lastReschedule
}

func (l *level) Width() int {
	return len(l.GetRows()[0])
}

func (l *level) Height() int {
	return len(l.GetRows())
}

func (l *level) IsWalkable(location Location) bool {
	return !l.IsOutsideMap(location) && (l.HasShallowWaterAt(location) || l.SolidGroundAt(location))
}

func (l *level) SolidGroundAt(location Location) bool {
	x, y := location.X, location.Y
	terrain := []rune(l.GetRows()[y])[x]
	return !l.IsOutsideMap(location) && (terrain == rune('.') || terrain == rune(','))
}
func (l *level) HasDeepWaterAt(location Location) bool {
	x, y := location.X, location.Y
	terrain := []rune(l.GetRows()[y])[x]
	return !l.IsOutsideMap(location) && terrain == rune('W')
}

func (l *level) HasShallowWaterAt(location Location) bool {
	x, y := location.X, location.Y
	terrain := []rune(l.GetRows()[y])[x]
	return !l.IsOutsideMap(location) && terrain == rune('w')
}

func (l *level) HasWallAt(location Location) bool {
	x, y := location.X, location.Y
	terrain := []rune(l.GetRows()[y])[x]
	return !l.IsOutsideMap(location) && terrain == rune('#')
}

func (l *level) IsOutsideMap(location Location) bool {
	x, y := location.X, location.Y
	return y < 0 || x < 0 || y >= l.Height() || x >= l.Width()
}

func (l *level) SpaceOccupied(location Location) (bool, *Actor) {
	for e := l.actors.Front(); e != nil; e = e.Next() {
		a := e.Value.(*Actor)
		if a.Location.X == location.X && a.Location.Y == location.Y {
			return true, a
		}
	}
	return false, nil
}

/* AddActor adds a actor at a position that corresponds to it's turn to act. A prerequsite is that the actor list is sorted already.*/
func (l *level) AddActor(a *Actor) {
	if l.actors.Len() > 0 {
		for e := l.actors.Front(); e != nil; e = e.Next() {
			if a.Time < e.Value.(*Actor).Time {
				l.actors.InsertBefore(a, e)
				return
			}
		}
		l.actors.PushBack(a)
	} else {
		l.actors.PushFront(a)
	}
	l.lastReschedule.time = l.actors.Front().Value.(*Actor).Time
	l.lastReschedule.actor = l.actors.Front().Value.(*Actor)
}

func (l *level) RemoveActor(a *Actor) error {
	for e := l.actors.Front(); e != nil; e = e.Next() {
		if a == e.Value.(*Actor) {
			l.actors.Remove(e)
			return nil
		}
	}
	return errors.New("Could not find actor to remove")
}

func (l *level) GetRows() []string {
	return l.rows
}
func (l *level) GetActors() list.List {
	return l.actors
}

/* NextToAct returns the actor which is next in turn to act. A prerequsite is that
all items in the actor list is sorted except the first two */
func (l *level) NextToAct() *Actor {
	if l.actors.Len() > 1 {
		first, second := l.actors.Front(), l.actors.Front().Next()
		//only reorder if something have changed
		if l.lastReschedule.time < first.Value.(*Actor).Time || l.lastReschedule.actor != first.Value {
			l.lastReschedule.time = first.Value.(*Actor).Time
			l.lastReschedule.actor = first.Value.(*Actor)
			if first.Value.(*Actor).Time >= second.Value.(*Actor).Time {
				removed := l.actors.Remove(first).(*Actor)
				l.AddActor(removed)
			}
		}
	}
	return l.actors.Front().Value.(*Actor)
}
