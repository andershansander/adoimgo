package game

import (
	"log"
)

const alertnessWeight float32 = 3.0

func lookAround(a *Actor, s *State, detectionFunction func(a1, a2 *Actor) bool) []Event {
	l := CurrentLevel(s)
	actors := l.GetActors()
	resultingEvents := make([]Event, 0)
	for o := actors.Front(); o != nil; o = o.Next() {
		other := o.Value.(*Actor)
		if a == other {
			continue
		}

		if a.CanSee(other.Location) {
			if a.memory.HaveNoticed(other) {
				log.Println(a.Sign + " sees " + other.Sign)
				a.memory.Memorize(other)
			} else if detectionFunction(a, other) {
				log.Println(a.Sign + " detected " + other.Sign)
				a.memory.Memorize(other)
				if other == s.Player {
					resultingEvents = append(resultingEvents, Event{
						Message: a.Sign + " detected you!",
						Secret:  !s.Player.CanSee(other.Location),
					})
				} else {
					var target string
					if s.Player.CanSee(other.Location) {
						target = other.Sign
					} else {
						target = "something"
					}
					resultingEvents = append(resultingEvents, Event{
						Message: a.Sign + " detected " + target,
						Secret:  !s.Player.CanSee(a.Location),
					})
				}
			}
		} else {
			if a.memory.HaveNoticed(other) {
				log.Println(a.Sign + " lost sight of " + other.Sign)
				a.memory.otherActors[other.Id].noticed = false
				resultingEvents = append(resultingEvents, Event{
					Message: a.Sign + " lost sight of you",
					Secret:  !s.Player.CanSee(a.Location),
				})
			}
		}
	}
	return resultingEvents
}
