package game

type actorMemory struct {
	actor         *Actor
	worldLocation WorldLocation
	location      Location
	enemy         bool
	noticed       bool
}

type memory struct {
	otherActors map[ActorID]*actorMemory
}

func (m *memory) HaveNoticed(other *Actor) bool {
	return m.KnowsAbout(other) && m.otherActors[other.Id].noticed
}

func (m *memory) KnowsAbout(other *Actor) bool {
	_, ok := m.otherActors[other.Id]
	return ok
}

func (m *memory) Memorize(other *Actor) {
	if m.KnowsAbout(other) {
		m.otherActors[other.Id].noticed = true
		m.otherActors[other.Id].location = other.Location
	} else {
		m.otherActors[other.Id] = &actorMemory{
			noticed:  true,
			location: other.Location,
			actor:    other,
		}
	}
}

func createEmptyMemory() *memory {
	return &memory{
		otherActors: make(map[ActorID]*actorMemory),
	}
}
