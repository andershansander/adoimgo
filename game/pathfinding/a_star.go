package pathfinding

import (
	"container/heap"
	"log"
	"math"
	"strconv"
)

func Target(x, y int) target {
	return &location{x, y}
}

func StartPoint(x, y int) startPoint {
	return &location{x, y}
}

type Square struct {
	Traversable  bool
	MovementCost int
	Heuristic    int
	X            int
	Y            int
}

type node struct {
	index          int
	s              Square
	shortestPathTo []step
	cost           int
	open           bool
	closed         bool
}

type target interface {
	X() int
	Y() int
}

type startPoint interface {
	X() int
	Y() int
}

func toString(sp startPoint) string {
	return "x: " + strconv.Itoa(sp.X()) + ", y: " + strconv.Itoa(sp.Y())
}

type step interface {
	X() int
	Y() int
}

type location struct {
	x, y int
}

func (l *location) X() int {
	return l.x
}

func (l *location) Y() int {
	return l.y
}

func removeIndex(squares []Square, index int) ([]Square, Square) {
	sqr := squares[index]
	return append(squares[0:index], squares[index+1:len(squares)]...), sqr
}

func CreatePath(m [][]Square, sp startPoint, t target) (bool, []step) {
	log.Println("finding path from " + toString(sp) + " and " + toString(t))
	nodes := make([][]*node, len(m))
	for y, row := range m {
		nodes[y] = make([]*node, len(row))
		for x, square := range row {
			nodes[y][x] = &node{s: square, open: false, closed: false, cost: 0, shortestPathTo: make([]step, 0)}
		}
	}
	pq := &priorityQueue{}
	heap.Init(pq)
	current := nodes[sp.Y()][sp.X()]
	current.cost = 0
	heap.Push(pq, current)
	for {
		if pq.Len() == 0 {
			return false, nil
		}
		current = heap.Pop(pq).(*node)
		current.closed = true
		if current.s.X == t.X() && current.s.Y == t.Y() {
			return true, current.shortestPathTo
		}
		for _, neighbor := range traversableNeighbours(nodes, current.s.X, current.s.Y) {
			if neighbor.closed {
				continue
			}
			cost := current.cost + (neighbor.s.MovementCost+current.s.MovementCost)/2
			if (neighbor.open && cost <= neighbor.cost) || !neighbor.open {
				neighbor.open = true
				neighbor.cost = cost
				neighbor.shortestPathTo = append(current.shortestPathTo, &location{x: neighbor.s.X, y: neighbor.s.Y})
				heap.Push(pq, neighbor)
			}
		}
	}
}

func traversableNeighbours(nodes [][]*node, currX, currY int) []*node {
	toVisit := make([]*node, 0)
	for y := int(math.Max(float64(currY-1), 0)); y < int(math.Min(float64(len(nodes)), float64(currY+2))); y++ {
		for x := int(math.Max(float64(currX-1), 0)); x < int(math.Min(float64(len(nodes[y])), float64(currX+2))); x++ {
			if nodes[y][x].s.Traversable && (currX != x || currY != y) {
				toVisit = append(toVisit, nodes[y][x])
			}
		}
	}
	return toVisit
}
